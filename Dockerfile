ARG PYTHON_VERSION
FROM python:${PYTHON_VERSION}

ARG REPLACE_PIP_CONF
ARG REQUIREMENTS_FILE

COPY conf/pip.conf /tmp/pip.conf
RUN if [ "${REPLACE_PIP_CONF}" = "true" ]; then \
        mv /tmp/pip.conf /etc/pip.conf; \
    fi

COPY "${REQUIREMENTS_FILE}" /tmp/requirements.txt
RUN pip install --upgrade pip setuptools \
    && pip install gunicorn \
    && pip install -r /tmp/requirements.txt

WORKDIR /src